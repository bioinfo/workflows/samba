#!/usr/bin/env python3


from __future__ import print_function
from loguru import logger
import rich_click as click

CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])


@click.version_option("1.1", prog_name="NANOPORE_03c_filter_count_table.py")
@click.command(context_settings=CONTEXT_SETTINGS)
@click.option('-t', '--tsv', type=click.Path(exists=True), required=True, help='Raw matrix for all samples (tsv)')
@click.option('-s', '--specie', type=click.FloatRange(0, 1), default=0.97,
              help='Minimal alignment identity to assign at specie level [0.97]')
@click.option('-g', '--genus', type=click.FloatRange(0, 1), default=0.94,
              help='Minimal alignment identity to assign at genus level [0.94]')
@click.option('-f', '--family', type=click.FloatRange(0, 1), default=0.90,
              help='Minimal alignment identity to assign at family level [0.90]')              
@click.option('-o', '--out', type=str, required=True, help='Output file')
def main(tsv, specie, genus, family, out):

    if not (genus < specie):
        raise click.UsageError(
            "The percentage of identity must be decreasing and without overlap: genus < specie")
    
    if not (family < genus):
        raise click.UsageError(
            "The percentage of identity must be decreasing and without overlap: family < genus")

    header, filter_matrix = filter_tsv(tsv, specie, genus, family)
    write_filtered_matrix(filter_matrix, header, out)


def filter_tsv(tsv, specie_t, genus_t, family_t):
    filter_matrix = {}
    threshold = 4

    with open(tsv, 'r') as matrix:
        header = next(matrix)
        for line in matrix:
            columns = line.strip().split('\t')
            read_id = columns[0]
            read_taxo = columns[-4]
            read_status = columns[-3]
            read_identity = columns[-2]
            # Ignore other than Assigned reads
            if read_status != 'Assigned':
                filter_matrix[read_id] = line[:-1]
            else:
                # To be consistent with NANOPORE_03_count_table.py rank option
                # if is modified by the user. Default Family (5).
                taxo_split = read_taxo.split(';')
                if len(taxo_split) < threshold:
                    columns[-3] = 'Low_taxonomy'
                    filter_matrix[read_id] = '\t'.join(columns)
                else:
                    read_taxo_chk, read_status_chk = check_assign_level(float(read_identity), read_taxo, specie_t,
                                                                        genus_t, family_t)
                    columns[-4] = read_taxo_chk
                    columns[-3] = read_status_chk
                    filter_matrix[read_id] = '\t'.join(columns)
    return header[:-1], filter_matrix


def check_assign_level(identity, taxo, specie_t, genus_t, family_t):
    taxo_levels = taxo.split(';')  # Split taxonomy into levels

    # Identify the rank of species and genus dynamically
    specie_rank = len(taxo_levels)  # Species is the last available rank
    genus_rank = specie_rank - 1    # Genus is one level above species
    family_rank = specie_rank - 2    # Family is two level above species

    if identity >= specie_t:
        taxo_chk = taxo  # Keep full taxonomy
        status_chk = 'Assigned_specie'
    elif specie_t > identity >= genus_t:
        taxo_chk = ';'.join(taxo_levels[:genus_rank])  # Keep only up to detected genus rank
        status_chk = 'Assigned_genus'
    elif genus_t > identity >= family_t:
        taxo_chk = ';'.join(taxo_levels[:family_rank])  # Keep only up to detected genus rank
        status_chk = 'Assigned_family'
    else:
        taxo_chk = taxo  # Keep full taxonomy but mark as ambiguous
        status_chk = 'Ambiguous'

    return taxo_chk, status_chk


def write_filtered_matrix(filter_matrix, header, out):
    with open(out, 'w') as filtered:
        filtered.write(f'{header}\n')
        for read in filter_matrix.values():
            filtered.write(f'{read}\n')


if __name__ == '__main__':
    main()
