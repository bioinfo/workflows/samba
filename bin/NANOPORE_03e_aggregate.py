#!/usr/bin/env python3

from loguru import logger
import rich_click as click


CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])


@click.version_option("1.0", prog_name="NANOPORE_03e_aggregate.py")
@click.command(context_settings=CONTEXT_SETTINGS)
@click.option('-t', '--tsv', type=click.Path(exists=True), required=True, help='Matrix filtered')
@click.option('-o', '--out', type=str, required=True, help='Output file')
def main(tsv, out):
    logger.info(f"Starting analysis for {tsv}")
    sum_matrix, header = summing(tsv)
    logger.info(f"Write output to {out}")
    write_aggregate(sum_matrix, header, out)
    logger.info(f"Finished")


def summing(tsv):
    sum_matrix = {}
    with open(tsv, 'r') as matrix:
        header = next(matrix)
        for line in matrix:
            columns = line.strip().split("\t")
            taxonomy = columns[-4]
            assignation = columns[-3]
            if assignation.startswith("Assigned"):
                if taxonomy not in sum_matrix:
                    sum_matrix[taxonomy] = [0] * len(line.split()[1:-4])
                zipped_list = zip(sum_matrix[taxonomy], list(map(int, columns[1:-4])))
                sum_matrix[taxonomy] = [sum(item) for item in zipped_list]
    return sum_matrix, header


def write_aggregate(matrix, header, out):
    tab = '\t'
    aggregate = open(out, 'w')
    aggregate.write(f'Taxonomy\t{tab.join(header.split()[1:-4])}\n')
    for taxo_id in matrix:
        aggregate.write(f'{taxo_id}\t{tab.join(map(str, matrix[taxo_id]))}\n')
    aggregate.close()


if __name__ == '__main__':
    main()
