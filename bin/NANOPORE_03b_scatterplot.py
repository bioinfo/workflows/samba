#!/usr/bin/env python3

import os
import matplotlib.pyplot as plt
from loguru import logger
import rich_click as click

CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])


@click.version_option("1.0", prog_name="NANOPORE_03b_scatterplot.py")
@click.command(context_settings=CONTEXT_SETTINGS)
@click.option('-m', '--matrix', type=click.Path(exists=True), required=True,
              help='Path to matrix from NANOPORE_03_count_table.py')
@click.option('-o', '--out', type=str, required=False, help='Output file')
def main(matrix, out):
    scatter = {
        'Assigned': [[], []],
        'Ambiguous': [[], []],
        'Low_taxonomy': [[], []],
        'Multihit': [[], []],
        'Unmapped': [[], []],
    }
    bar = {'Assigned': 0, 'Ambiguous': 0, 'Low_taxonomy': 0, 'Multihit': 0, 'Unmapped': 0}
    with open(matrix, 'r') as lr_data:
        for read in lr_data:
            read_type = read.split()[-3]
            read_identity = read.split()[-2]
            read_coverage = read.split()[-1]
            if read_type == 'Unmapped':
                scatter[read_type][0].append(0)
                scatter[read_type][1].append(0)
                bar[read_type] += 1
            elif read_type != 'Assignation':
                scatter[read_type][0].append(float(read_identity) * 100)
                scatter[read_type][1].append(float(read_coverage) * 100)
                bar[read_type] += 1

    # Categories repartition for barplot
    snum = sum(bar.values())
    for read_type in bar:
        bar[read_type] = bar[read_type] / snum * 100

    # Define plot layout
    mosaic = """
                 AABBE
                 CCDDE
                 """
    # Initiate plot
    fig, axs = plt.subplot_mosaic(mosaic, figsize=(14, 10))

    # Scatter plot for each category
    for i, (mosaic, type_name, color) in enumerate([('A', 'Assigned', '#0073C2FF'), ('B', 'Ambiguous', '#EFC000FF'),
                                                    ('C', 'Low_taxonomy', '#F36D43'),
                                                    ('D', 'Multihit', '#D53E4E')]):
        axs[mosaic].scatter(scatter[type_name][0], scatter[type_name][1], color=color)
        axs[mosaic].set_xlabel('%identity')
        axs[mosaic].set_xlim(50, 100)
        axs[mosaic].set_ylim(0, 100)
        axs[mosaic].set_xlabel("%Identity", fontsize=12, fontweight='bold')
        axs[mosaic].set_ylabel("%Coverage", fontsize=12, fontweight='bold')
        axs[mosaic].grid(True, color='#909090', alpha=0.6, linewidth=0.4)

    # Global barplot
    bottom_values = 0
    for key, color in [('Unmapped', '#949A9E'), ('Multihit', '#D53E4E'),
                       ('Low_taxonomy', '#F36D43'), ('Ambiguous', '#EFC000FF'), ('Assigned', '#0073C2FF')]:
        axs['E'].bar('Type', bar[key], bottom=bottom_values, label=' '.join(key.split('_')), color=color)
        axs['E'].text(0, bottom_values + bar[key] / 2.5, f"{bar[key]:.2f}", ha='center', va='bottom', fontsize=12,
                 color='white')
        bottom_values = bottom_values + bar[key]
    axs['E'].set_ylim(0, 100)
    axs['E'].set_xlabel("Type", fontsize=12, fontweight='bold')
    handles, labels = axs['E'].get_legend_handles_labels()
    axs['E'].legend(reversed(handles), reversed(labels), fontsize=12, bbox_to_anchor=(1.0, 0.65))

    # Rendering and save plot
    plt.tight_layout()
    name = os.path.splitext(os.path.basename(matrix))[0]
    if out:
        name = out
    plt.savefig(f'{name}.stats.png', bbox_inches='tight', dpi=600)
    plt.close()


if __name__ == '__main__':
    main()
