#!/usr/bin/env python3

import os
import matplotlib.pyplot as plt
from loguru import logger
import rich_click as click

CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])


@click.version_option("1.0", prog_name="NANOPORE_03d_barplot.py")
@click.command(context_settings=CONTEXT_SETTINGS)
@click.option('-m', '--matrix', type=click.Path(exists=True), required=True,
              help='Path to filtered matrix')
@click.option('-o', '--out', type=str, required=False, help='Output file')
def main(matrix, out):
    scatter = {
        'Ambiguous': [[], []],
        'Low_taxonomy': [[], []],
        'Assigned_family': [[], []],
        'Assigned_genus': [[], []],
        'Assigned_specie': [[], []],
        'Multihit': [[], []],
        'Unmapped': [[], []],
    }
    bar = {'Assigned_family': 0, 'Assigned_genus': 0, 'Assigned_specie': 0, 'Ambiguous': 0, 'Low_taxonomy': 0,
           'Multihit': 0, 'Unmapped': 0}
    with open(matrix, 'r') as lr_filt_data:
        next(lr_filt_data)
        for read in lr_filt_data:
            read_type = read.split()[-3]
            read_identity = read.split()[-2]
            read_coverage = read.split()[-1]
            if read_type == 'Unmapped':
                scatter[read_type][0].append(0)
                scatter[read_type][1].append(0)
                bar[read_type] += 1
            else:
                scatter[read_type][0].append(float(read_identity) * 100)
                scatter[read_type][1].append(float(read_coverage) * 100)
                bar[read_type] += 1

    # Categories repartition for barplot
    snum = sum(bar.values())
    for read_type in bar:
        bar[read_type] = bar[read_type] / snum * 100

    # Initiate plot
    fig, axs = plt.subplots(1, 1, figsize=(4, 5))

    # Global barplot
    bottom_values = 0
    for key, color in [('Unmapped', '#949A9E'),
                       ('Multihit', '#D53E4E'),
                       ('Low_taxonomy', '#F36D43'),
                       ('Ambiguous', '#EFC000FF'),
                       ('Assigned_family', '#80B1D3'),  # Nouvelle couleur
                       ('Assigned_genus', '#AADDA4'),
                       ('Assigned_specie', '#66C2A5')]:
        axs.bar('Type', bar[key], bottom=bottom_values, label=' '.join(key.split('_')), color=color)
        axs.text(0, bottom_values + bar[key] / 2.5, f"{bar[key]:.2f}", ha='center', va='bottom', fontsize=8, color='white')
        bottom_values = bottom_values + bar[key]
    axs.set_ylim(0, 100)
    axs.set_xlabel("Type", fontsize=12, fontweight='bold')
    handles, labels = axs.get_legend_handles_labels()
    axs.legend(reversed(handles), reversed(labels), bbox_to_anchor=(1.0, 0.65))

    # Rendering and save plot
    plt.tight_layout()
    name = os.path.splitext(os.path.basename(matrix))[0]
    if out:
        name = out
    plt.savefig(f'{name}.stats.png', bbox_inches='tight', dpi=600)
    plt.close()


if __name__ == '__main__':
    main()