process compress_result {

    input:
        val(alpha_ok)
        val(beta_ok)
        val(intersect_ok)

    output:
        path("SAMBA_report.zip"), emit: report_zip

    script:
    """
    cd ${params.outdir}/${params.report_dirname} && zip -r SAMBA_report.zip * 
    cd -
    ln -sf ${params.outdir}/${params.report_dirname}/SAMBA_report.zip
    """
}
