![samba](./assets/samba_v4_logo.png)

[![SAMBA version](https://img.shields.io/badge/samba%20version-v4.0.0-red?labelColor=000000)](https://gitlab.ifremer.fr/bioinfo/workflows/samba)
[![Nextflow](https://img.shields.io/badge/nextflow-%E2%89%A522.10.0-23aa62.svg?labelColor=000000)](https://www.nextflow.io/)
[![Run with](https://img.shields.io/badge/run%20with-singularity%20&%20docker-1d355c.svg?labelColor=000000)]()
[![Developers](https://img.shields.io/badge/Developers-SeBiMER-yellow?labelColor=000000)](https://sebimer.ifremer.fr/)

## Introduction

SAMBA is a FAIR scalable workflow integrating, into a unique tool, state-of-the-art bioinformatics and statistical methods to conduct reproducible eDNA analyses using [Nextflow](https://www.nextflow.io) (Di Tommaso *et al.*, 2017). SAMBA performs complete metabarcoding analysis by :
- verifying integrity of raw reads and metadata
- processing data using commonly used procedure with [QIIME 2](https://qiime2.org/) (version 2022.11 ; Bolyen *et al.*, 2019) and [DADA2](https://docs.qiime2.org/2022.11/plugins/available/dada2/?highlight=dada2) (Callahan *et al.*, 2016)
- adding new steps to build high quality ASV count tables by:
    - ASV clustering relying on either [dbOTU3](https://github.com/swo/dbotu3) (Olesen *et al.*, 2017) and [swarm](https://github.com/torognes/swarm) (Mahé *et al.*, 2022)
    - [microDecon](https://github.com/donaldtmcknight/microDecon) (McKnight *et al.*, 2019)
- conducting extended statistical and ecological analyses using homemade Rscript
- producing a full dynamic HTML report including resources used, commands executed, intermediate results, statistical analyses and figures

Optional processes can also be performed through SAMBA such as analysis of compositions of microbiomes using [ANCOM-BC](https://bioconductor.org/packages/devel/bioc/vignettes/ANCOMBC/inst/doc/ANCOMBC.html) (Lin & Peddada, 2020) and functional prediction analysis using [PICRUSt2](https://github.com/picrust/picrust2) (Douglas *et al.*, 2020). 

The SAMBA pipeline can run tasks across multiple compute infrastructures in a very portable manner. It comes with singularity containers making installation trivial and results highly reproducible.

![SAMBA Workflow](./docs/images/samba-v3.0.png)

## Quick Start

i. Install [`nextflow`](https://www.nextflow.io/docs/latest/getstarted.html#installation)

ii. Install [`Singularity`](https://www.sylabs.io/guides/3.0/user-guide/) for full pipeline reproducibility. 

*NOTE:* If your HPC nodes don't have any internet access. Please download before any workflow run the singularity images available on Ifremer FTP at [https://data-dataref.ifremer.fr/bioinfo/ifremer/sebimer/tools/SAMBA/v4/](https://data-dataref.ifremer.fr/bioinfo/ifremer/sebimer/tools/SAMBA/v4/). Then set the `$NXF_SINGULARITY_CACHEDIR` environment variable to the path where you just downloaded the images.  

iii. Download the pipeline and test it on a minimal dataset with a single command

* for Illumina short reads test:
```bash
nextflow run main.nf -profile illumina_test,singularity
```

* for Nanopore reads test:
```bash
nextflow run main.nf -profile nanopore_test,singularity
```

> To use samba on a computing cluster, it is necessary to provide a configuration file for your system. For some institutes, this one already exists and is referenced on [nf-core/configs](https://github.com/nf-core/configs#documentation). If so, you can simply download your institute custom config file and simply use `-c <institute_config_file>` in your command. This will enable `singularity` and set the appropriate execution settings for your local compute environment.

iv. Start running your own analysis!

* for Illumina analysis:

```bash
nextflow run main.nf -profile singularity,illumina [-c <institute_config_file>]
```

* for Nanopore analysis:

```bash
nextflow run main.nf -profile singularity,nanopore [-c <institute_config_file>]
```

See the [samba documentation](https://bioinfo.gitlab-pages.ifremer.fr/workflows/samba-docs) for a complete description of the workflow and tutorials.

## Credits

SAMBA is written by [SeBiMER](https://sebimer.ifremer.fr/), the Bioinformatics Core Facility of [IFREMER](https://wwz.ifremer.fr/en/).

## Contributions

We welcome contributions to the pipeline. If such case you can do one of the following:
* Use issues to submit your questions 
* Fork the project, do your developments and submit a pull request
* Contact us (see email below) 

## Support

For further information or help, don't hesitate to get in touch with the samba developpers: 

![samba email](assets/samba-email-address-image.png)
