/*
 * -------------------------------------------------
 *  Nextflow test config file for processes options
 * -------------------------------------------------
 * Defines general paths for input files and
 * parameters for samba processes
 */

params {

    /* --------------------
     *  General parameters
     * --------------------
     */

    // Analyzed project name
    projectName = ""
    // Output directory to publish workflow results
    outdir = "${baseDir}/results/${projectName}"

    // Input parameters
    // sample file must be in XLS format
    excel_sample_file = ""

    // Type of data
    data_type = "illumina"
    singleEnd = false
    raw_read_length = "250"

    /* ------------------------------
     *  Merge multiple Illumina runs
     * ------------------------------
     */

    // Path to the directory containing all the table qza files for each run
    merge_table_dir = ""
    
    // Path to the directory containing all the sequence qza files for each run
    merge_repseq_dir = ""

    /* -------------------------------------
     *  ASV taxonomic assignment using 
     *  QIIME2 RDP-like program
     * -------------------------------------
     * pre-formatted databases can be download here: 
     * https://data-dataref.ifremer.fr/bioinfo/ifremer/sebimer/sequence-set/SAMBA/v4/databases/
     */

    // Name of the taxonomic database. Can be: silva (7 tax ranks), pr2-4 (8 ranks), pr2-5 (9 ranks) or MARE-MAGE (9 ranks)
    db_name = ""

    //Path to a trained Naive Bayes QIIME 2 classifier
    database = ""

    /* -------------------------------------
     *  Differential abundance analysis
     *  using ANCOM-BC
     * -------------------------------------
     */

    //Activation
    ancombc_enable = false

    //Set to true ff you want to use custom categorical group as reference to identify differential abundances
    //otherwise the first value of the variable(s) tested will be taken as a reference
    use_custom_reference = false
        //Custom sample group value to use as reference. Syntax: "column_name::column_value"
        reference_level = ""

    //Variable to analyse for differential abundance
    //can be one variable: "column_name"
    //can be several variables to be analyse separately: "column_name_1,column_name2,..."
    //can be several variables to be analyse in interaction: "column_name_1+column_name2"
    ancombc_formula = ""

    //Statistical parameters
        //method for adjusted pvalue
        p_adj_method = "holm"

        //number of iterations for the E-M algorithm
        max_iter = "100"

        //significant pvalue threshold
        alpha = "0.05"

    /* -------------------------------------
     *  Functional predictions using
     *  PICRUSt2
     * -------------------------------------
     * Only for 16S analysis
     */

    //Activation
    picrust2_enable = false

    //Gene families to predict. Can be: COG, EC, KO, PFAM, TIGRFAM
    traits_db = "EC,KO"

    //Max nsti value accepted. By default: NSTI cut-off of 2 should eliminate junk sequences
    nsti = "0.2"

    //HSP method of your choice. By default: 'mp' the most accurate prediction method. Faster method: 'pic'
    hsp_method = "mp"

    //Minimum number of reads across all samples for each input ASV
    min_reads = "1"

    //Minimum number of samples that an ASV needs to be identfied within
    min_samples = "1"

    //Variables of interest (comma separated list)
    picrust2_tested_variable = ""

    //Pathway description file
    secondary_level_mapfile = "${baseDir}/containers/picrust2-2.5.1/metacyc_pathways_info_prokaryotes_sec_level.tsv"

    /* -----------------------------
     *  Statistical analyses
     * -----------------------------
     */

    //Normalisation method(s) used
    //can be "rarefaction", "css", "deseq2" or multiple methods: "rarefaction,css,deseq2"
    normalisation_type = "rarefaction,css,deseq2"

    // Variable(s) to test for alpha and beta diversity
    //can be one variable: "varname"
    //can be several variables to be analyse separately: "varname1,varname2,..."
    stat_var = ""

    // Number of taxa to represent
    taxa_nb = "10"

}
